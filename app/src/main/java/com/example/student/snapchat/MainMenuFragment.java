package com.example.student.snapchat;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class MainMenuFragment extends Fragment {

    static final int REQUEST_CHOOSE_PHOTO = 2;

    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

            String[] menuItems = {
                    "Photos",
                    "Friends",
                    "Add Friend",
                    "Friend Requests",
                    "Camera",
                    "Send Picture",
                    "Incoming Photos"
            };

        Button logOutButton = (Button)view.findViewById(R.id.logOutButton);
        logOutButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Backendless.UserService.logout(new AsyncCallback<Void>() {
                                                    @Override
                                                    public void handleResponse(Void response) {
                                                        Toast.makeText(getActivity(), "You Logged Out!", Toast.LENGTH_SHORT).show();
                                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                                        startActivity(intent);
                                                    }

                                                    @Override
                                                    public void handleFault(BackendlessFault fault) {
                                                        Toast.makeText(getActivity(), "You failed to log out, LOSER", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }
                                        });

            ListView listView = (ListView) view.findViewById(R.id.mainMenu);

            ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                    getActivity(),
                    android.R.layout.simple_list_item_1,
                    menuItems
            );

            listView.setAdapter(listViewAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener()

            {
                @Override
                public void onItemClick (AdapterView < ? > parent, View view,int position, long id){
                if (position == 0) {
                    Intent intent = new Intent(getActivity(), OtherActivity1.class);
                    startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(getActivity(), FriendListActivity.class);
                    startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(getActivity(), AddFriendActivity.class);
                    startActivity(intent);
                } else if (position == 3) {
                    Intent intent = new Intent(getActivity(), FriendRequestActivity.class);
                    startActivity(intent);
                }else if (position == 4) {
                        Intent intent = new Intent(getActivity(), CameraActivity.class);
                        startActivity(intent);
                } if(position == 5) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                                REQUEST_CHOOSE_PHOTO);
                    } else if (position == 6){
                        Intent intent = new Intent(getActivity(), InboxActivity.class);
                        startActivity(intent);
                    }
            }
            }

            );
            // Inflate the layout for this fragment
            return view;
        }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CHOOSE_PHOTO){
            if(resultCode == Activity.RESULT_OK){
                Uri uri = data.getData();
                Intent intent = new Intent(getActivity(), FriendListActivity.class);
                intent.putExtra("ImageURI", uri);
                startActivity(intent);
            }
        }
    }
}

