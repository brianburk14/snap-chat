package com.example.student.snapchat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ActivityReceiver extends BroadcastReceiver {
    public ActivityReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(Constants.BROADCAST_ADD_FRIEND_SUCCESS)) {
            Toast.makeText(context, "Added Friend", Toast.LENGTH_SHORT).show();
        }
        else if(action.equals(Constants.BROADCAST_ADD_FRIEND_FAILURE)){
            Toast.makeText(context, "This person doesn't exist!", Toast.LENGTH_LONG).show();
        } else if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS)){
            Toast.makeText(context, "Friend Request Sent", Toast.LENGTH_LONG).show();
        } else if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_FAILURE)){
            Toast.makeText(context, "Friend Request Error, this person probably doesn't exists!", Toast.LENGTH_LONG).show();
        }
    }
}
