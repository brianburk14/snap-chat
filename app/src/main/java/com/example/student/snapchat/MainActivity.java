package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "FC0A8643-B36A-0295-FFB6-CD6D5D6C7D00";
    public static final String SECRET_KEY = "0E791E7B-2189-B271-FFDF-8D62693C0200";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MainMenuFragment mainMenu = new MainMenuFragment();
        //getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();

        //LoginMenuFragment loginMenu = new LoginMenuFragment();
        //getSupportFragmentManager().beginTransaction().add(R.id.container, loginMenu).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == ""){
            LoginMenuFragment loginMenu =  new LoginMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginMenu).commit();
        }
        else{
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }
    }
}
